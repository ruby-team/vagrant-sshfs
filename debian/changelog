vagrant-sshfs (1.3.7-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Fix field name typo in debian/copyright (X-Comment ⇒ Comment).
  * Update standards version to 4.6.2, no changes needed.

  [ Hans-Christoph Steiner ]
  * New upstream version 1.3.7

 -- Hans-Christoph Steiner <hans@eds.org>  Sun, 05 Feb 2023 23:52:24 +0100

vagrant-sshfs (1.3.6-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

  [ Hans-Christoph Steiner ]
  * New upstream version 1.3.6

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 15 Dec 2021 22:29:55 +0100

vagrant-sshfs (1.3.5-1) unstable; urgency=medium

  [ Cédric Boutillier ]
  * [ci skip] Update team name
  * [ci skip] Add .gitattributes to keep unwanted files out of the source package

  [ Hans-Christoph Steiner ]
  * New upstream version 1.3.5

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 20 Nov 2020 13:14:55 +0100

vagrant-sshfs (1.3.4-1) unstable; urgency=medium

  [ Hans-Christoph Steiner ]
  * New upstream version
  * Rules-Requires-Root: no

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.4.1, no changes needed.

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 26 May 2020 17:39:26 +0200

vagrant-sshfs (1.3.1-1) unstable; urgency=medium

  * New upstream version 1.3.1
  * point Vcs tags to salsa.debian.org
  * make debian/watch check the PGP signatures
  * update to current gemwatch
  * use HTTPS for debian/copyright URL
  * Standards-Version: 4.3.0: remove get-orig-source

 -- Hans-Christoph Steiner <hans@eds.org>  Sat, 26 Jan 2019 22:20:29 +0000

vagrant-sshfs (1.3.0-2) unstable; urgency=medium

  * Team upload.
  * debian/rules: use rubygems installation layout (Closes: #854517)
  * Add missing dependency on vagrant
  * Add simple autopkgtest smoke test

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 14 Feb 2017 09:59:55 -0200

vagrant-sshfs (1.3.0-1) unstable; urgency=medium

  * New upstream release

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 06 Jan 2017 22:52:50 +0100

vagrant-sshfs (1.2.0-1) unstable; urgency=low

  * Initial release. (Closes: #836875)

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 06 Sep 2016 19:47:17 +0200
